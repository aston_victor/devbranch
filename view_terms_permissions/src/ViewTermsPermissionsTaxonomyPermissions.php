<?php

namespace Drupal\view_terms_permissions;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Class ViewTermsPermissionsTaxonomyPermissions.
 */
class ViewTermsPermissionsTaxonomyPermissions {
  use StringTranslationTrait;

  /**
   * Returns an array of vocabulary permissions.
   *
   * @return array
   *   The vocabulary permissions.
   */
  public function permissions() {
    $permissions = [];

    foreach (Vocabulary::loadMultiple() as $vocabulary) {
      $permissions += [
        "view terms permissions {$vocabulary->id()}" => [
          'title' => $this->t('View terms from @vocabulary vocabulary', ['@vocabulary' => $vocabulary->label()]),
          'description' => $this->t('Provides access to the terms from @vocabulary vocabulary.', ['@vocabulary' => $vocabulary->label()]),
        ],
      ];
    }

    return $permissions;
  }

}
