<?php

namespace Drupal\node_updates_mailing\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Class NodeUpdatesMailingConfigForm.
 *
 * @package Drupal\node_updates_mailing\Form
 */
class NodeUpdatesMailingConfigForm extends FormBase {

  /**
   * Returns the state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a NodeUpdatesMailingConfigForm form.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   Defines the interface for the state system.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_updates_mailing_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['help'] = [
      '#type' => 'html_tag',
      '#tag' => 'b',
      '#value' => $this->t('This module uses revisions. Please, enable revisions for content types.'),
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email recipient'),
      '#required' => TRUE,
      '#default_value' => $this->state->get('node_updates_mailing_email'),
    ];

    $form['hours'] = [
      '#type' => 'number',
      '#title' => $this->t('Hours'),
      '#min' => '1',
      '#required' => TRUE,
      '#default_value' => $this->state->get('node_updates_mailing_hours'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->state->setMultiple([
      'node_updates_mailing_email' => $form_state->getValue('email'),
      'node_updates_mailing_hours' => $form_state->getValue('hours'),
    ]);
  }

}
