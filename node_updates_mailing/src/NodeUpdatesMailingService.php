<?php

namespace Drupal\node_updates_mailing;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\Core\Language\LanguageInterface;

/**
 * Class NodeUpdatesMailingService.
 */
class NodeUpdatesMailingService {
  use StringTranslationTrait;

  /**
   * Returns the plugin.manager.mail service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Returns the entity.query service.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $query;

  /**
   * Returns the date.formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $date;

  /**
   * Returns the state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Returns the renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a NodeUpdatesMailingService service.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   Defines an interface for pluggable mail back-ends.
   * @param \Drupal\Core\Entity\Query\QueryFactory $query
   *   Factory class Creating entity query objects.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date
   *   Provides an interface defining a date formatter.
   * @param \Drupal\Core\State\StateInterface $state
   *   Defines the interface for the state system.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Defines an interface for turning a render array into a string.
   */
  public function __construct(MailManagerInterface $mailManager, QueryFactory $query, DateFormatterInterface $date, StateInterface $state, RendererInterface $renderer) {
    $this->mailManager = $mailManager;
    $this->query = $query;
    $this->date = $date;
    $this->state = $state;
    $this->renderer = $renderer;
  }

  /**
   * Send e-mail to recipient.
   */
  public function sendMail($time) {
    if ($recipient = $this->state->get('node_updates_mailing_email')) {
      $query = $this->query->get('node');
      $query->condition('changed', $time, '>=');

      if ($nids = $query->execute()) {
        $headers = [
          $this->t('Node title'),
          $this->t('Time of update'),
          $this->t('Email of user who updated'),
        ];

        $rows = [];
        foreach ($nids as $nid) {
          $node = Node::load($nid);

          $changed = $node->get('changed')->getString();

          $email = '-';
          if ($user_id = $node->get('revision_uid')->getString()) {
            if ($user = User::load($user_id)) {
              $email = $user->getEmail();
            }
          }

          $rows[] = [
            'label' => $node->toLink($node->label(), 'canonical', ['absolute' => TRUE])->toString(),
            'changed' => $this->date->format($changed),
            'mail' => $email,
          ];
        }

        $message = [
          '#type' => 'table',
          '#header' => $headers,
          '#rows' => $rows,
        ];

        $params = [
          'subject' => $this->t('Nodes were updated'),
          'message' => $this->renderer->render($message),
        ];

        $this->mailManager->mail('node_updates_mailing', 'node_updates_mailing_mail', $recipient, LanguageInterface::LANGCODE_SITE_DEFAULT, $params, NULL, TRUE);
      }
    }
  }

}
