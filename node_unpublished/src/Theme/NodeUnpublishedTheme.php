<?php

namespace Drupal\node_unpublished\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 * Display unpublished node with admin theme.
 */
class NodeUnpublishedTheme implements ThemeNegotiatorInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $node = $route_match->getParameter('node');
    if (!empty($node)) {
      return !$node->isPublished();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $config = \Drupal::config('system.theme');
    return $config->get('admin');
  }

}
